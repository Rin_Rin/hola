numbers = ["6","4","3","8","2","9"]
has = true
aal = numbers.length - 1
while has
    has = false
    for i in 0...aal
        has |= numbers[i] > numbers[i + 1] 
        numbers[i], numbers[i + 1] = numbers[i + 1], numbers[i] if numbers[i] > numbers [i + 1]
    end
    aal -= 1
end
puts numbers.join(', ')
